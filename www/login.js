$(function(){

  // = action
  
  function lockScreen() {
    var s = $('#SearchConditions');
    var w = s.outerWidth(true);
    var h = s.offset().top + s.outerHeight(true);

    var divTag = $('<div />').attr("id", 'screen');
    divTag.css("z-index", "9998")
          .css("position", "absolute")
          .css("top", "0px")
          .css("left", "0px")
          .css("right", "0px")
          .css("bottom", "0px")
          .css("width", "100%")
          .css("min-width", String(w) + 'px')
          .css("height", "100%")
          .css("min-height", String(h) + 'px')
          .css("background-color", "gray")
          .css("opacity", "0.8");
    $('body').append(divTag);
  }
 
  function unlockScreen() {
    $("#screen").fadeOut('slow');
    $("#screen").remove();
  }

  function openDialog() {
    lockScreen();

    var dialog = $('#login-dialog');
    var leftPos = (($(window).width() - dialog.outerWidth(true)) / 2);
    var topPos = (($(window).height() - dialog.outerHeight(true)) / 2);
    dialog.css({"left": leftPos + "px"});
    dialog.css({"top": topPos + "px"});
    dialog.show();
    switchLoginButtonEnability(false);
  }

  function closeDialog() {
    var dialog = $('#login-dialog');
    dialog.fadeOut('slow');

    unlockScreen();
  }

  function showErrorMessage() {
      $('#username').val('');
      $('#password').val('');
      maintainLoginButtonEnability();
      
      $('#messageArea').addClass('error').text('ログインに失敗しました。');
  }

  function switchLoginButtonEnability(enebility) {
    if (enebility && $('#tryLogin').prop('disabled')) {
      $('#tryLogin').animate({
        backgroundColor: "#ffffff"
      }, 100, 'linear', function () {
        $('#tryLogin').prop('disabled', false);
      });
    } else if (!enebility && !$('#tryLogin').prop('disabled')) {
      $('#tryLogin').animate({
        backgroundColor: "#f5f5f5"
      }, 100, 'linear', function () {
        $('#tryLogin').prop('disabled', true);
      });
    }
  }

  function maintainLoginButtonEnability() {
    var enebility = $('#username').val().length > 0 &&
                    $('#password').val().length > 0;
    switchLoginButtonEnability(enebility);
  }

  var counter = 0;
  function tryLogin() {
    console.log('tryLogin');
    Shiny.onInputChange('tryLogin', ++counter);
  }

  // = bind

  $('#username').on('keyup', maintainLoginButtonEnability);
  $('#password').on('keyup', maintainLoginButtonEnability);
  $('#tryLogin').on('click', tryLogin);
  Shiny.addCustomMessageHandler('loginResult', function (message) {
    console.log('loginResult:');
    console.dir(message);
    if (message.result === 'TRUE') {
      closeDialog();
    } else {
      showErrorMessage();
    }
  });

  // = initilize

  openDialog();

});
