source('shinyWithLogin.R', encoding = 'UTF-8')

shinyUIWithLogin(navbarPage(
    "REDCap症例抽出ツール",
    theme = shinytheme("cerulean"),
    tabPanel(
        "症例抽出",
        tags$head(tags$link(
            rel = "stylesheet", type = "text/css", href = "style.css"
        )),
        useShinyjs(),
        
        titlePanel("症例抽出"),
        wellPanel(
            id = "SearchConditions",
            div(
                id = "row",
                actionButton(
                    inputId = "filter",
                    label = "検索"
                ),
                actionButton(
                    inputId = "clear",
                    label = "クリア"
                ),
                hidden(downloadButton(
                    outputId = "download_button",
                    label = "ダウンロード"
                ))
            ),
            div(
                id = "row",
                selectInput(
                    inputId = "facility",
                    label = "施設名",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            df.dags[["idxs.dag"]],
                            df.dags[["names.dag"]]
                        )
                    )
                ),
                textOutput(
                    outputId = "id_fac"
                ),
                div(
                    id = "text_id_fac",
                    "－"
                ),
                numericInput(
                    inputId = "id_l",
                    label = "Record ID",
                    value = NA,
                    min = 0,
                    max = 999
                )
            ),
            div(
                id = "row",
                selectInput(
                    inputId = "type",
                    label = "病型",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dis_dx_type_1", 1),
                            myGetChoices("dis_dx_type_1")
                        )
                    )
                ),
                hidden(checkboxInput(
                    inputId = "type_start_only",
                    label = "治療開始時のみ"
                ))
            ),
            div(
                id = "row",
                numericInput(
                    inputId = "age_lower",
                    label = "診断時年齢",
                    value = NA,
                    min = 0,
                    max = 999
                ),
                div(
                    id = "text_age_lower_1",
                    "歳"
                ),
                div(
                    id = "text_age_lower_2",
                    "～"
                ),
                numericInput(
                    inputId = "age_upper",
                    label = "　",
                    value = NA,
                    min = 0,
                    max = 999
                ),
                div(
                    id = "text_age_upper",
                    "歳"
                ),
                selectInput(
                    inputId = "gender",
                    label = "性別",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("gender", 1),
                            myGetChoices("gender")
                        )
                    )
                )
            ),
            div(
                id = "row",
                selectInput(
                    inputId = "outcome",
                    label = "転帰",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("outcome", 1),
                            myGetChoices("outcome")
                        )
                    )
                ),
                numericInput(
                    inputId = "term_lower",
                    label = "観察期間",
                    value = NA,
                    min = 0,
                    max = 999
                ),
                div(
                    id = "text_term_lower_1",
                    "ヶ月"
                ),
                div(
                    id = "text_term_lower_2",
                    "～"
                ),
                numericInput(
                    inputId = "term_upper",
                    label = "　",
                    value = NA,
                    min = 0,
                    max = 999
                ),
                div(
                    id = "text_term_upper",
                    "ヶ月"
                ),
                selectInput(
                    inputId = "iss",
                    label = "ISS 病期分類",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dx_iss_select", 1),
                            myGetChoices("dx_iss_select")
                        )
                    )
                )
            ),
            div(
                id = "row",
                selectInput(
                    inputId = "mprotein",
                    label = "血清M蛋白",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dx_blood_mprotein", 1),
                            myGetChoices("dx_blood_mprotein")
                        )
                    )
                ),
                hidden(selectInput(
                    inputId = "mprotein_kl",
                    label = "　",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dx_blood_kap_lam", 1),
                            myGetChoices("dx_blood_kap_lam")
                        )
                    )
                )),
                selectInput(
                    inputId = "bjp",
                    label = "尿BJP",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dx_uro_bjp", 1),
                            myGetChoices("dx_uro_bjp")
                        )
                    )
                ),
                hidden(selectInput(
                    inputId = "bjp_kl",
                    label = "　",
                    choices = c(
                        "すべて" = "allTRUE",
                        setNames(
                            myGetChoices("dx_uro_bjp_kap_lam", 1),
                            myGetChoices("dx_uro_bjp_kap_lam")
                        )
                    )
                ))
            ),
            div(
                id = "progress",
                "読み込み中…"
            ),
            hidden(div(
                id = "button_show",
                actionButton("show", "詳細条件")
            )),
            hidden(div(
                id = "blank",
                "　"
            )),
            hidden(div(
                id = "details",
                div(
                    id = "row",
                    selectInput(
                        inputId = "regimen",
                        label = "化学療法（レジメン）",
                        choices = c(
                            "すべて" = "allTRUE",
                            setNames(
                                myGetChoices("tx_regimen", 1),
                                myGetChoices("tx_regimen")
                            )
                        )
                    )
                ),
                div(
                    id = "row",
                    checkboxGroupInput(
                        inputId = "drug",
                        label = "化学療法（薬剤）",
                        choices = c(
                            setNames(
                                myGetChoices("tx_drug", 1),
                                myGetChoices("tx_drug")
                            )
                        ),
                        inline = T
                    )
                ),
                div(
                    id = "row",
                    selectInput(
                        inputId = "radiation",
                        label = "放射線治療",
                        choices = set_names(
                            myGetChoices("tx_rt_yn", 1),
                            myGetChoices("tx_rt_yn")
                        )
                    ),
                    selectInput(
                        inputId = "sct",
                        label = "移植",
                        choices = c(
                            "すべて" = "allTRUE",
                            setNames(
                                myGetChoices("tx_sct_auto_allo", 1),
                                myGetChoices("tx_sct_auto_allo")
                            )
                        )
                    ),
                    numericInput(
                        inputId = "cr_lower",
                        label = "診断時Cr",
                        value = NA,
                        step = 0.1
                    ),
                    div(
                        id = "text_cr_lower_1",
                        "mg/dL"
                    ),
                    div(
                        id = "text_cr_lower_2",
                        "～"
                    ),
                    numericInput(
                        inputId = "cr_upper",
                        label = "　",
                        value = NA,
                        step = 0.1
                    ),
                    div(
                        id = "text_cr_upper",
                        "mg/dL"
                    )
                ),
                div(
                    id = "row",
                    checkboxGroupInput(
                        inputId = "event",
                        label = "重大事象",
                        choices = c(
                            setNames(
                                myGetChoices("serious_event", 1),
                                myGetChoices("serious_event")
                            )
                        ),
                        inline = T
                    )
                ),
                div(
                    id = "row",
                    textInput(
                        inputId = "keyword",
                        label = "キーワード"
                    )
                ),
                actionButton("hide", "閉じる")
            ))
        ),
        hidden(wellPanel(
            id = "ExtractionResult",
            dataTableOutput("cases")
        ))
    ),
    tabPanel(
        "症例登録数",
        div(
            id = "progress",
            "読み込み中…"
        ),
        div(
            style = "font-size: 18px;",
            dataTableOutput("facilities")
        )
    )
))
